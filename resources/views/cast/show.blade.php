@extends('dashboard')

@section('content')
<div class="card m-2">
    <div class="m-2">
        <h2>Show Cast {{$post->id}}</h2>
        <hr>
        <h4>Nama: {{$post->nama}}</h4>
        <p>Umur: {{$post->umur}}</p>
        <p>Bio: {{$post->bio}}</p>
    </div> 
</div>
@endsection