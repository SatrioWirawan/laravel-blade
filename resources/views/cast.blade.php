@extends('dashboard')

@section('content')
<div class="card">
    <div class="card-header">
      <h1 class="card-title">Cast</h1>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Bio</th>
          <th>Action 
            <button type="button" id="" class="btn btn-outline-secondary border-1 p-1 ml-4">
            <a class="" href="" style="color: black;">
                <span class="fas fa-trash-alt"></span>
                Remove
            </a>
        </button></th>

        </tr>
        </thead>
        <tbody>
            {{-- @forelse ($post as $key=>$value) --}}
            <tr>
                {{-- <td>{{$key + 1}}</td>
                <td>{{$value->$nama}}</td>
                <td>{{$value->$umur}}</td>
                <td>{{$value->$bio}}</td> --}}
                <td>
                    <button type="button" id="" class="btn btn-outline-secondary border-1 p-1">
                        <a class="" href="" style="color: black;">
                            <span class="far fa-file-alt"></span>
                            show
                        </a>
                    </button>
                    <button type="button" id="" class="btn btn-outline-secondary border-1 p-1">
                        <a class="" href="" style="color: black;">
                            <span class="fas fa-edit"></span>
                            Edit
                        </a>
                    </button>
                    <button type="button" id="" class="btn btn-outline-secondary border-1 p-1">
                        <a class="" href="" style="color: black;">
                            <span class="fas fa-trash-alt"></span>
                            Remove
                        </a>
                    </button>
                </td>
            </tr>
            {{-- @empty --}}
            <tr>
                <td colspan="3">No data</td>
            </tr>
            {{-- @endforelse --}}
        </tbody>    
      </table>
    </div>
    <!-- /.card-body -->

</div>
<script src="../../plugins/datatables/jquery.dataTables.js"></script>
<script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
@push('table-script')
<script src="{{asset('')}}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{asset('')}}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@endsection